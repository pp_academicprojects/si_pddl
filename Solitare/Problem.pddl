(define (problem p1)
	(:domain Solitare)
	(:objects p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15)
	(:init
		
		(empty p1)
		
		(connected p1 p3 p6)
		(connected p1 p2 p4)
		
		(connected p2 p5 p9)
		(connected p2 p4 p7)
		
		(connected p3 p5 p8)
		(connected p3 p6 p10)
		
		(connected p4 p2 p1)
		(connected p4 p5 p6)
		(connected p4 p7 p11)
		(connected p4 p8 p13)
		
		(connected p5 p8 p12)
		(connected p5 p9 p14)
		
		(connected p6 p3 p1)
		(connected p6 p5 p4)
		(connected p6 p9 p13)
		(connected p6 p10 p15)
		
		
		(connected p7 p4 p2)
		(connected p7 p8 p9)
		
		(connected p8 p5 p3)
		(connected p8 p9 p10)
		
		(connected p9 p5 p2)
		(connected p9 p8 p7)
		
		(connected p10 p6 p3)
		(connected p10 p9 p8)
		
		(connected p11 p7 p4)
		(connected p11 p12 p13)
		
		(connected p12 p8 p5)
		(connected p12 p13 p14)
		
		(connected p13 p12 p11)
		(connected p13 p8 p4)
		(connected p13 p9 p6)
		(connected p13 p14 p15)
		
		(connected p14 p9 p5)
		(connected p14 p13 p12)
		
		(connected p15 p10 p6)
		(connected p15 p14 p13)
		
	)
	(:goal (and
		(not (empty p1))
		(empty p2)
		(empty p3)
		(empty p4)
		(empty p5)
		(empty p6)
		(empty p7)
		(empty p8)
		(empty p9)
		(empty p10)
		(empty p11)
		(empty p12)
		(empty p13)
		(empty p14)
		(empty p14))
	)
)