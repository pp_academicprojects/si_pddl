(define
	(domain Solitare)
	(:requirements :adl)
	(:predicates
		(empty ?x)
		(connected ?x ?y ?z)
	)
        ; przesu� klocek na pod�og�
	(:action strike
		:parameters (?x ?y ?z)
		:precondition
		(and
			(connected ?x ?y ?z)
			(not (empty ?x))
			(not (empty ?y))
			(empty ?z)
		)
		:effect
		(and
			(empty ?x)
			(empty ?y)
			(not (empty ?z))
		)
	)
)