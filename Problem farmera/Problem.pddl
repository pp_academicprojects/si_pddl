(define (problem p1)
	(:domain Problem-farmera)
	(:objects Farmer Koza Wilk Kapusta West East)
	(:init
		
		(farmer Farmer)
		
		(obiekt Koza)
		(obiekt Wilk)
		(obiekt Kapusta)
		
		(brzeg West)
		(brzeg East)
		
		(konflikt Koza Wilk)
		(konflikt Wilk Koza)
		(konflikt Koza Kapusta)
		(konflikt Kapusta Koza)
		
		(polozenie Farmer West)
		(polozenie Koza West)
		(polozenie Wilk West)
		(polozenie Kapusta West)
		
	)
	(:goal (and
		(polozenie Farmer East)
		(polozenie Koza East)
		(polozenie Wilk East)
		(polozenie Kapusta East)
		)
	)
)