begin_version
3
end_version
begin_metric
0
end_metric
10
begin_variable
var0
1
2
Atom new-axiom@0(east, kapusta)
<none of those>
end_variable
begin_variable
var1
1
2
Atom new-axiom@0(east, wilk)
<none of those>
end_variable
begin_variable
var2
1
2
Atom new-axiom@0(west, kapusta)
<none of those>
end_variable
begin_variable
var3
1
2
Atom new-axiom@0(west, wilk)
<none of those>
end_variable
begin_variable
var4
1
2
Atom new-axiom@1(east)
<none of those>
end_variable
begin_variable
var5
1
2
Atom new-axiom@1(west)
<none of those>
end_variable
begin_variable
var6
-1
2
Atom polozenie(farmer, east)
Atom polozenie(farmer, west)
end_variable
begin_variable
var7
-1
2
Atom polozenie(kapusta, east)
Atom polozenie(kapusta, west)
end_variable
begin_variable
var8
-1
2
Atom polozenie(koza, east)
Atom polozenie(koza, west)
end_variable
begin_variable
var9
-1
2
Atom polozenie(wilk, east)
Atom polozenie(wilk, west)
end_variable
4
begin_mutex_group
2
6 0
6 1
end_mutex_group
begin_mutex_group
2
7 0
7 1
end_mutex_group
begin_mutex_group
2
8 0
8 1
end_mutex_group
begin_mutex_group
2
9 0
9 1
end_mutex_group
begin_state
0
0
0
0
0
0
1
1
1
1
end_state
begin_goal
4
6 0
7 0
8 0
9 0
end_goal
8
begin_operator
przeplyn_sam east west farmer
1
4 1
1
0 6 0 1
0
end_operator
begin_operator
przeplyn_sam west east farmer
1
5 1
1
0 6 1 0
0
end_operator
begin_operator
przeplyn_z_rzecza east west farmer kapusta
1
0 1
2
0 6 0 1
0 7 0 1
0
end_operator
begin_operator
przeplyn_z_rzecza east west farmer koza
0
2
0 6 0 1
0 8 0 1
0
end_operator
begin_operator
przeplyn_z_rzecza east west farmer wilk
1
1 1
2
0 6 0 1
0 9 0 1
0
end_operator
begin_operator
przeplyn_z_rzecza west east farmer kapusta
1
2 1
2
0 6 1 0
0 7 1 0
0
end_operator
begin_operator
przeplyn_z_rzecza west east farmer koza
0
2
0 6 1 0
0 8 1 0
0
end_operator
begin_operator
przeplyn_z_rzecza west east farmer wilk
1
3 1
2
0 6 1 0
0 9 1 0
0
end_operator
12
begin_rule
1
7 0
3 0 1
end_rule
begin_rule
2
7 0
9 0
5 0 1
end_rule
begin_rule
1
7 1
1 0 1
end_rule
begin_rule
2
7 1
9 1
4 0 1
end_rule
begin_rule
1
8 0
2 0 1
end_rule
begin_rule
1
8 0
3 0 1
end_rule
begin_rule
1
8 0
5 0 1
end_rule
begin_rule
1
8 1
0 0 1
end_rule
begin_rule
1
8 1
1 0 1
end_rule
begin_rule
1
8 1
4 0 1
end_rule
begin_rule
1
9 0
2 0 1
end_rule
begin_rule
1
9 1
0 0 1
end_rule
