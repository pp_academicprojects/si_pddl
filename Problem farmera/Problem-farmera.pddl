(define 
	(domain Problem-farmera)
	(:requirements :adl)
	(:predicates
		(polozenie ?x ?y)
		(konflikt ?x ?y)
		(obiekt ?x)
		(farmer ?x)
		(brzeg ?x)
	)
	
	(:action Przeplyn_z_rzecza
		:parameters (?from ?to ?farmer ?obiekt_do_przeniesienia)
		:precondition
		(and
			(obiekt ?obiekt_do_przeniesienia)
			(brzeg ?from)
			(brzeg ?to)
			(not (= ?from ?to))
			(farmer ?farmer)
			(polozenie ?farmer ?from)
			(polozenie ?obiekt_do_przeniesienia ?from)
			(not (exists (?x ?y)
				(and 
					(not (= ?x ?y))
					(obiekt ?x)
					(obiekt ?y)
					(not (= ?x ?obiekt_do_przeniesienia))
					(not (= ?y ?obiekt_do_przeniesienia))
					(polozenie ?x ?from)
					(polozenie ?y ?from)
					(konflikt ?x ?y)
				)
			))
		)
		:effect
		(and
			(not (polozenie ?farmer ?from))
			(not (polozenie ?obiekt_do_przeniesienia ?from))
			(polozenie ?farmer ?to)
			(polozenie ?obiekt_do_przeniesienia ?to)
		)
	)
	
	(:action Przeplyn_sam 
		:parameters (?from ?to ?farmer)
		:precondition
		(and
			(brzeg ?from)
			(brzeg ?to)
			(farmer ?farmer)
			(not (= ?from ?to))
			(polozenie ?farmer ?from)
			(not (exists (?x ?y)
				(and 
					(not (= ?x ?y))
					(obiekt ?x)
					(obiekt ?y)
					(polozenie ?x ?from)
					(polozenie ?y ?from)
					(konflikt ?x ?y)
				)
			))
		)
		:effect
		(and
			(not (polozenie ?farmer ?from))
			(polozenie ?farmer ?to)
		)
	)
)









