(define 
	(domain Sokoban)
	(:requirements :adl)
	(:predicates
		(poziomo ?x ?y)
		(pionowo ?x ?y)
		(paczka ?x)
		(robot ?x)
		(cel ?x)
	)

	;IDZ PIONOWO
	(:action idzPionowo
		:parameters (?x ?y)
		:precondition 
		(and
			(not (= ?x ?y))
			(pionowo ?x ?y)
			(robot ?x)
			(not (paczka ?y))
		)
		:effect
		(and
			(not (robot ?x))
			(robot ?y)
		)
	)
	
	;IDZ POZIOMO
	(:action idzPoziomo
		:parameters (?x ?y)
		:precondition
		(and
			(not (= ?x ?y))
			(poziomo ?x ?y)
			(robot ?x)
			(not (paczka ?y))
		)
		:effect 
		(and
			(not (robot ?x))
			(robot ?y)
		)
	)
	
	;PCHAJ POZIOMO
	(:action pchajPoziomo
		:parameters (?x ?y ?z)
		:precondition
		(and
			(not (= ?x ?y))
			(not (= ?x ?z))
			(not (= ?y ?z))
			(poziomo ?x ?y)
			(poziomo ?y ?z)
			(robot ?x)
			(paczka ?y)
			(not (paczka ?z))
		)
		:effect 
		(and
			(not (robot ?x))
			(robot ?y)
			(not (paczka ?y))
			(paczka ?z)
		)
	)
	
	;PCHAJ PIONOWO
	(:action pchajPionowo
		:parameters (?x ?y ?z)
		:precondition
		(and
			(not (= ?x ?y))
			(not (= ?x ?z))
			(not (= ?y ?z))
			(pionowo ?x ?y)
			(pionowo ?y ?z)
			(robot ?x)
			(paczka ?y)
			(not (paczka ?z))
		)
		:effect 
		(and
			(not (robot ?x))
			(robot ?y)
			(not (paczka ?y))
			(paczka ?z)
		)
	)
	
)