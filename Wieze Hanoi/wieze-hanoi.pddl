(define
	(domain wieze-hanoi)
	(:requirements :adl)
	(:predicates
		(on-top ?x ?y)	
		(krazek ?x)
		(palik ?x)
		(clear ?x)
		(na-paliku ?x ?y)
	)
	
	(:action przesun-na-pusty
		
)