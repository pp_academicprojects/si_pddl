(define
	(domain Podroznik-planet)
	(:requirements :adl)
	(:predicates
		(path_opened ?x)
		(free_path ?x ?y)
		(limited_path ?x ?y ?z)
		(position ?x)
		(empty ?x)
		(connected ?x ?y ?z)
	)
	
	(:action use_paid_path
		:parameters (?x ?y)
		:precondition 
		(and
			(position ?x)
			(exists (?x ?y ?z)
				(and 
					(limited_path ?x ?y ?z)
					(path_opened ?z)
				)
			)
		)
		:effect
		(and
		
		)
        ; przesu� klocek na pod�og�
	(:action strike
		:parameters (?x ?y ?z)
		:precondition
		(and
			(connected ?x ?y ?z)
			(not (empty ?x))
			(not (empty ?y))
			(empty ?z)
		)
		:effect
		(and
			(empty ?x)
			(empty ?y)
			(not (empty ?z))
		)
	)
)