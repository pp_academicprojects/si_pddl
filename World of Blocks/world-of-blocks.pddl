(define
	(domain world-of-blocks)
	(:requirements :adl)
	(:predicates
		(on-top ?x ?y)
		(on-floor ?x)
		(clear ?x)
		(armempty)
		(holding ?x)
	)
	
	;Kladziemy z x na y
	(:action stack
		:parameters (?x ?y)
		:precondition
		(and
			(not (armempty))
			(holding ?x)
			(clear ?y)
		)
		:effect
		(and
			(clear ?x)
			(armempty)
			(not (holding ?x))
			(on-top ?x ?y)
			(not (clear ?y))
		)
	)
	
	;zdejmujemy ?x z ?y
	(:action unstack
		:parameters (?x ?y)
		:precondition
		(and 
			(armempty)
			(clear ?x)
			(on-top ?x ?y)
		)
		:effect
		(and 
			(not (armempty))
			(clear ?y)
			(not (on-top ?x ?y))
			(holding ?x)
		)
	)
	
	;kladziemy ?x na podloge
	(:action putdowm
		:parameters (?x)
		:precondition
		(and
			(not (armempty))
			(holding ?x)
		)
		:effect
		(and
			(armempty)
			(not (holding ?x))
			(clear ?x)
			(on-floor ?x)
		)
	)
	
	;podnosimy ?x z podloza
	(:action pickup
		:parameters (?x)
		:precondition
		(and 
			(armempty)
			(clear ?x)
			(on-floor ?x)
		)
		:effect 
		(and 
			(not (on-floor ?x))
			(not (armempty))
			(holding ?x)
		)
	)
	
	
        ; przesu� klocek na pod�og�
	;(:action move-to-floor
;		:parameters (?x ?z)
;		:precondition
;		(and
;			(clear ?x)
;			(on-top ?x ?z)
;		)
;		:effect
;		(and
;			(not (on-top ?x ?z))
;			(on-floor ?x)
;			(clear ?z)
;		)
;	)
	
 ;       ; przesu� klocek na inny kocek
	;(:action move-to-block
	;	:parameters (?x ?y)
	;	:precondition
;		(and
	;		(clear ?x)
	;		(clear ?y)
	;	)
	;	:effect
;		(and
	;		(on-top ?x ?y)
	;		(not (clear ?y))
	;		(not (on-floor ?x))
	;	)
	;)
)