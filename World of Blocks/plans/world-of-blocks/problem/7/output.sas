begin_version
3
end_version
begin_metric
0
end_metric
5
begin_variable
var0
-1
2
Atom armempty()
<none of those>
end_variable
begin_variable
var1
-1
2
Atom clear(a)
<none of those>
end_variable
begin_variable
var2
-1
5
Atom holding(a)
Atom on-floor(a)
Atom on-top(a, a)
Atom on-top(a, b)
Atom on-top(a, c)
end_variable
begin_variable
var3
-1
5
Atom holding(b)
Atom on-floor(b)
Atom on-top(b, a)
Atom on-top(b, b)
Atom on-top(b, c)
end_variable
begin_variable
var4
-1
5
Atom holding(c)
Atom on-floor(c)
Atom on-top(c, a)
Atom on-top(c, b)
Atom on-top(c, c)
end_variable
4
begin_mutex_group
4
0 0
2 0
3 0
4 0
end_mutex_group
begin_mutex_group
5
2 0
2 1
2 2
2 3
2 4
end_mutex_group
begin_mutex_group
5
3 0
3 1
3 2
3 3
3 4
end_mutex_group
begin_mutex_group
5
4 0
4 1
4 2
4 3
4 4
end_mutex_group
begin_state
0
1
1
1
2
end_state
begin_goal
5
0 0
1 0
2 3
3 4
4 1
end_goal
24
begin_operator
pickup a
1
1 0
2
0 0 0 1
0 2 1 0
0
end_operator
begin_operator
pickup b
0
2
0 0 0 1
0 3 1 0
0
end_operator
begin_operator
pickup c
0
2
0 0 0 1
0 4 1 0
0
end_operator
begin_operator
putdowm a
0
3
0 0 1 0
0 1 -1 0
0 2 0 1
0
end_operator
begin_operator
putdowm b
0
2
0 0 1 0
0 3 0 1
0
end_operator
begin_operator
putdowm c
0
2
0 0 1 0
0 4 0 1
0
end_operator
begin_operator
stack a a
1
1 0
2
0 0 1 0
0 2 0 2
0
end_operator
begin_operator
stack a b
0
3
0 0 1 0
0 1 -1 0
0 2 0 3
0
end_operator
begin_operator
stack a c
0
3
0 0 1 0
0 1 -1 0
0 2 0 4
0
end_operator
begin_operator
stack b a
1
1 0
2
0 0 1 0
0 3 0 2
0
end_operator
begin_operator
stack b b
0
2
0 0 1 0
0 3 0 3
0
end_operator
begin_operator
stack b c
0
2
0 0 1 0
0 3 0 4
0
end_operator
begin_operator
stack c a
1
1 0
2
0 0 1 0
0 4 0 2
0
end_operator
begin_operator
stack c b
0
2
0 0 1 0
0 4 0 3
0
end_operator
begin_operator
stack c c
0
2
0 0 1 0
0 4 0 4
0
end_operator
begin_operator
unstack a a
1
1 0
2
0 0 0 1
0 2 2 0
0
end_operator
begin_operator
unstack a b
1
1 0
2
0 0 0 1
0 2 3 0
0
end_operator
begin_operator
unstack a c
1
1 0
2
0 0 0 1
0 2 4 0
0
end_operator
begin_operator
unstack b a
0
3
0 0 0 1
0 1 -1 0
0 3 2 0
0
end_operator
begin_operator
unstack b b
0
2
0 0 0 1
0 3 3 0
0
end_operator
begin_operator
unstack b c
0
2
0 0 0 1
0 3 4 0
0
end_operator
begin_operator
unstack c a
0
3
0 0 0 1
0 1 -1 0
0 4 2 0
0
end_operator
begin_operator
unstack c b
0
2
0 0 0 1
0 4 3 0
0
end_operator
begin_operator
unstack c c
0
2
0 0 0 1
0 4 4 0
0
end_operator
0
